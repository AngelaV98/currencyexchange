import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "tachyons";
import { Provider } from "react-redux";
import axios from "axios";
import store from "./store.js";
import CurrencyExchange from "./CurrencyExchange";

function App() {
  return (
    <div className="App vh-100 flex items-center">
      <Provider store={store}>
        <CurrencyExchange />
      </Provider>
    </div>
  );
}

export default App;
