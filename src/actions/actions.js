import axios from "axios";

export const exchangeCurrency = data => dispatch => {
  axios({
    method: "GET",
    url: "https://currency-exchange.p.rapidapi.com/exchange",
    headers: {
      "content-type": "application/octet-stream",
      "x-rapidapi-host": "currency-exchange.p.rapidapi.com",
      "x-rapidapi-key": "551d467205msh1f17caebbd5b2f4p11de78jsnec95496c993b"
    },
    params: data
  })
    .then(res => {
      dispatch({
        type: "EXCHANGE_CURRENCY",
        payload: (res.data * +data.q).toFixed(4)
      });
    })
    .catch(err => dispatch({ type: "ERROR", payload: err }));
};
