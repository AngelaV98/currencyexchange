import React from "react";
import { connect } from "react-redux";
import {exchangeCurrency} from './actions/actions';

class CurrencyExchange extends React.Component {
  state = {
    q: "1",
    from: "USD",
    to: "EUR"
  };
  componentDidMount(){
    this.onExchange();
  }
  onExchange = e => {
    let data = { ...this.state };
    this.props.exchangeCurrency(data);
  };
  onChange = e => {
    this.setState({ q: e.target.value });
  };
  onValueSelect = e => {
    this.setState({ from: e.target.value });
  };
  onExchangeSelect = e => {
    this.setState({ to: e.target.value });
  };

  render() {
    return (
      <div className="w-70 bg-green pv2 center  mt5">
        <h1 className = 'f1 white'>Currency exchange</h1>
        <div className="flex justify-center">
          <input
            className="pa1"
            onChange={this.onChange}
            placeholder="Enter the value"
          />
        <select className="pa1 ml2" value={this.state.from} onChange={this.onValueSelect}>
            <option>USD</option>
            <option>EUR</option>
            <option>RUB</option>
            <option>AMD</option>
            <option>CHF</option>
          </select>
          <button
            className="bg-blue ba pa1 white do ml2"
            type="button"
          >{`<-->`}</button>
        <select className="ml2 pa1" value={this.state.to} onChange={this.onExchangeSelect}>
            <option>USD</option>
            <option>EUR</option>
            <option>RUB</option>
            <option>AMD</option>
            <option>CHF</option>
          </select>
        </div>
        <button
          type="button"
          className="bg-light-purple pointer ba pa2 white f3 dim mt1"
          onClick={this.onExchange}
        >
          Exchange
        </button>
        <h3>
          {this.state.q}&nbsp;
          {this.state.from} = {this.props.currency} {this.state.to}
        </h3>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currency: state.currency
});

export default connect(
mapStateToProps,{exchangeCurrency}
// mapDispatchToProps
)(CurrencyExchange);
