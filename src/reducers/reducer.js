let initialState = {
  currency: ""
};

const reducer = (state = initialState, action) => {
  let newState = { ...state };
  if (action.type === "EXCHANGE_CURRENCY") {
    newState.currency = action.payload;
  }
  return newState;
};

export default reducer;
